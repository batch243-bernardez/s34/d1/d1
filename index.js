// Use the require() directive to load the express module or package
	const express = require('express');

// Create application using express function
// "app" will be the server
	const app = express();

// For our application server to run, we need a "port" to listen to
	const port = 3000;

// Methods used from expressJS will add a "middlewares"
// "express.____()" is a middlewares
// API management is one of the common applicataions of middlewares
// It allows your app to read json data
	app.use(express.json());

// Allows your app to read data from forms
// ".urlencoded" parse the incoming requests
	app.use(express.urlencoded({extended:true}))


// [SECTION] Creating Routes
/*
	- Express has methods corresponding to each HTTP method
	- Syntax:
		app.METHOD
*/

// Create a GET method route
// This route expects to receive a GET request at the base of the URI "/"
// The full base URI for our local application for this route is "http://localhost:3000"
	app.get("/", (request,response) => {
		response.send("Hello World");
	})

// Create another GET route
// This route expects to receive a GET request at the URI "/hello"
	/*app.get("/hello", (request,response) => {
		response.send("Hello from the /hello endpoint!")
	})*/

// Create POST route
// This route expects to receive a POST request at the URI "/hello"
// request.body contains the contents or data of the request body
	app.use("/hello", (request,response) => {
		response.send(`Hello there ${request.body.firstName} ${request.body.lastName}`);
	})

// Create ARRAY to use as our mock database
// An array that will store our user objects when the "/signup" route is accessed
	let users = [];
	app.post("/signup", (request,response) => {
		console.log(request.body)

		if(request.body.username !== "" && request.body.password !== ""){
			users.push(request.body);
			response.send(`User ${request.body.username} is successfully registered`);
			console.log(users);
		}

		else{
			response.send("Please input both username and password")
		}
	})

// Create PUT route to update password
// This route expects to receive a PUT request at the URI "/change-password"
	app.put("/change-password", (request,response) => {
		
		let message;
		for(let i = 0; i<users.length; i++){
			if(request.body.username == users[i].username){
				users[i].password = request.body.password

				message = `User ${request.body.username}'s password has been updated.`

				break;
			}

			else{
				message = "User does not exist";
			}
		}
			response.send(message);
	})

/*// If the username and password are not complete, an error message will be sent back to the client or postman

else{
	response.send("Please input both username and password!")
}*/

/*PUT is a technique of altering resources when the client transmits data that revamps the whole resource. PATCH is a technique for transforming the resources when the client transmits partial data that will be updated without changing the whole data.*/






	app.listen(port, () => console.log(`Server running at port ${port}`));